import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { EmployeeService } from '../employee.service';
import { Employee } from '../shared/employee';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss']
})
export class EmployeeDetailsComponent implements OnInit {
  employee: Employee;
  employeeForm: FormGroup;
  isNewEmployee = false;

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private location: Location,
    private fb: FormBuilder
  ) {
    this.employeeForm = this.fb.group({
      name: ['', Validators.required],
      designation: ['', Validators.required],
      salary: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getHero();
  }

  isFieldValid(field: string) {
    return (
      !this.employeeForm.get(field).valid &&
      this.employeeForm.get(field).touched
    );
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (!id) {
      this.employee = new Employee();
      this.isNewEmployee = true;
      return;
    }
    this.employeeService.getEmployee(id).subscribe(employee => {
      if (employee) {
        this.employee = employee;
        this.employeeForm.patchValue({
          name: employee.name,
          designation: employee.designation,
          salary: employee.salary
        });
      }
    });
  }

  save(): void {
    this.employee.name = this.employeeForm.value.name;
    this.employee.designation = this.employeeForm.value.designation;
    this.employee.salary = this.employeeForm.value.salary;

    if (this.isNewEmployee) {
      this.employeeService
        .addEmployee(this.employee)
        .subscribe(() => this.goBack());
    } else {
      this.employeeService
        .updateEmployee(this.employee)
        .subscribe(() => this.goBack());
    }
  }

  goBack(): void {
    this.location.back();
  }
}
