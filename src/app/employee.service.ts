import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Employee } from './shared/employee';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private employeesUrl = `${environment.apiUrl}/employees`;

  // cache employees in service
  private employees: Employee[];

  constructor(private http: HttpClient) { }

  getEmployees(): Observable<Employee[]> {
    // if employees already feteched, return it
    if (this.employees) {
      return of(this.employees);
    }
    // if employees loading for the first time
    return this.http.get<Employee[]>(this.employeesUrl)
      .pipe(
        tap(employees => this.employees = employees),
        catchError(this.handleError('getEmployees', []))
      );
  }

  getEmployee(id): Observable<Employee> {
    if (this.employees) {
      const employee = this.employees.find( e => e.id === id);
      if (employee) {
        return of(employee);
      }
    }
    return this.http.get<Employee>(`${this.employeesUrl}/${id}`)
      .pipe(
        catchError(this.handleError('getEmployee', null))
      );
  }

  updateEmployee(employee: Employee): Observable<any> {
    return this.http.put(`${this.employeesUrl}/${employee.id}`, employee, httpOptions).pipe(
      catchError(this.handleError<any>('updateEmployee'))
    );
  }

  addEmployee (employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.employeesUrl, employee, httpOptions)
      .pipe(
        tap(emp => {
          this.employees.push(emp);
        }),
        catchError(this.handleError<Employee>('addEmployee'))
      );
  }

  deleteEmployee (employee: Employee | number): Observable<Employee> {
    const id = typeof employee === 'number' ? employee : employee.id;
    const url = `${this.employeesUrl}/${id}`;

    return this.http.delete<Employee>(url, httpOptions).pipe(
      catchError(this.handleError<Employee>('deleteEmployee'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a EmployeeService message */
  private log(message: string) {
    console.log(`EmployeeService: ${message}`);
  }
}
}
