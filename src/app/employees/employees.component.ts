import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Router, NavigationExtras } from '@angular/router';

import { Employee } from '../shared/employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  displayedColumns: string[] = ['name', 'designation', 'salary', 'actions'];
  dataSource: MatTableDataSource<Employee>;

  employees: Employee[];

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
  ) {
    this.dataSource = new MatTableDataSource(this.employees);
  }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(employees => {
        this.employees = employees;
        this.dataSource.data = this.employees;
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  editEmployee(id) {
    this.router.navigate(['details', id]);
  }

  deleteEmployee(id) {
    this.employeeService.deleteEmployee(id)
      .subscribe(_ => {
        this.employees = this.employees.filter(e => e.id !== id);
        this.dataSource.data = this.employees;
      });
  }

}
