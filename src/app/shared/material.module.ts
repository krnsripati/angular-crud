import { NgModule } from '@angular/core';
import {
  MatToolbarModule,
  MatTableModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
} from '@angular/material';

const modules = [
  MatToolbarModule,
  MatTableModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
];

@NgModule({
  imports: [
    ...modules,
  ],
  exports: [
    ...modules,
  ],
})
export class MaterialModule { }
